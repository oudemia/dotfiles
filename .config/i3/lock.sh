#!/bin/sh
. ~/.cache/wal/colors.sh

RING_C=$color1
HIGHLIGHT_C=$color2
TEXT_C=$color2
INSIDE_C=$background
WRONG_C=$color7
VERIFIYING_C=$color1
SIGMA=10 

i3lock \
--indicator                     \
--clock                         \
--timestr="%H:%M:%S"            \
--datestr="%Y-%m-%d"            \
--timecolor=$TEXT_C             \
--datecolor=$TEXT_C             \
\
--ignore-empty-password         \
--blur=$SIGMA                   \
\
--insidecolor=$INSIDE_C         \
--insidevercolor=$INSIDE_C      \
--insidewrongcolor=$INSIDE_C    \
\
--ringcolor=$RING_C             \
--ringvercolor=$VERIFIYING_C    \
--ringwrongcolor=$WRONG_C       \
\
--line-uses-inside              \
--separatorcolor=$RING_C        \
\
--keyhlcolor=$HIGHLIGHT_C       \
--bshlcolor=$HIGHLIGHT_C        \
--verifcolor=$VERIFIYING_C      \
--wrongcolor=$WRONG_C           \
\

# --veriftext="Drinking verification can..."
# --wrongtext="Nope!"
# --textsize=20
# --modsize=10
# --timefont=comic-sans
# --datefont=monofur
# etc
