"custom commands

:command TME TableModeEnable 

:command TMD TableModeDisabled

" Clear the search highlights according to
" https://stackoverflow.com/questions/657447/vim-clear-last-search-highlighting#comment5906101_657484
command C let @/=""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" mapping commands

let mapleader = " "

let maplocalleader = " "

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"basic settings

set nocompatible            " disable compatibility to old-time vi

set inccommand=nosplit      " Preview commands

set showmatch               " show matching brackets

set ignorecase              " case insensitive matching

set hlsearch                " highlight search results

set tabstop=4               " number of columns occupied by a tab character

set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing

set expandtab               " converts tabs to white space

set shiftwidth=4            " width for autoindents

set autoindent              " indent a new line the same amount as the line just typed

set number                  " add line numbers

set cc=81                   " set an 80 column border for good coding style

set spelllang=de,en,cjk
set spell

filetype plugin indent on   " allows auto-indenting depending on file type

syntax on                   " syntax highlighting

set cursorline

set termguicolors

set clipboard=unnamedplus

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" plugins

call plug#begin()

Plug 'donRaphaco/neotex', { 'for': 'tex' }

Plug 'scrooloose/nerdcommenter' " auto commenting via /cc /cu

Plug 'preservim/nerdtree'       " file system explorer

Plug 'jiangmiao/auto-pairs' " automatic quote and bracket completion

Plug 'tpope/vim-surround'  "  altering e.g. html tags

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' } "autocomplete

Plug 'vim-airline/vim-airline' "powerline equivalent

Plug 'dhruvasagar/vim-table-mode' "creating tables with ascii elements

Plug 'vim-airline/vim-airline-themes'

Plug 'abnt713/vim-hashpunk'

Plug 'cseelus/vim-colors-lucid'

Plug 'artur-shaik/vim-javacomplete2'

Plug 'lervag/vimtex' "supports writing latex documents

Plug 'dense-analysis/ale' "syntax checking and semantic error highlighting

Plug 'roxma/nvim-yarp'

Plug 'roxma/vim-hug-neovim-rpc'

Plug 'Shougo/neosnippet.vim'

Plug 'Shougo/neosnippet-snippets'

Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

Plug 'hasufell/ghcup.vim' "ghcup

Plug 'rbgrouleff/bclose.vim' "ghcup for nvim

Plug 'parsonsmatt/intero-neovim' "run GHCi console process inside neovim

Plug 'neovimhaskell/haskell-vim'

Plug 'neomake/neomake' "for error logging when using intero, running programs asynchronously

Plug 'reedes/vim-colors-pencil'

call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" fun with colors
colorscheme pencil
let g:pencil_higher_contrast_ui = 0
:set background=dark
let g:airline_theme = 'pencil'
let g:airline_powerline_fonts = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"setup deoplete 
let g:deoplete#enable_at_startup = 1

let g:LanguageClient_serverCommands = {
  \ 'c': ['clangd'],
  \ 'cpp': ['clangd'],
  \ }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" neomake setup

call neomake#configure#automake('w')


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" default language client key bindings
function SetLSPShortcuts()
  nnoremap <leader>ld :call LanguageClient#textDocument_definition()<CR>
  nnoremap <leader>lr :call LanguageClient#textDocument_rename()<CR>
  nnoremap <leader>lf :call LanguageClient#textDocument_formatting()<CR>
  nnoremap <leader>lt :call LanguageClient#textDocument_typeDefinition()<CR>
  nnoremap <leader>lx :call LanguageClient#textDocument_references()<CR>
  nnoremap <leader>la :call LanguageClient_workspace_applyEdit()<CR>
  nnoremap <leader>lc :call LanguageClient#textDocument_completion()<CR>
  nnoremap <leader>lh :call LanguageClient#textDocument_hover()<CR>
  nnoremap <leader>ls :call LanguageClient_textDocument_documentSymbol()<CR>
  nnoremap <leader>lm :call LanguageClient_contextMenu()<CR>
endfunction()

augroup LSP
  autocmd!
  autocmd FileType cpp,c call SetLSPShortcuts()
augroup END


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" javacomplete2 settings
autocmd FileType java setlocal omnifunc=javacomplete#Complete

nmap <F4> <Plug>(JavaComplete-Imports-AddSmart)

imap <F4> <Plug>(JavaComplete-Imports-AddSmart)

nmap <F5> <Plug>(JavaComplete-Imports-Add)

imap <F5> <Plug>(JavaComplete-Imports-Add)

nmap <F6> <Plug>(JavaComplete-Imports-AddMissing)

imap <F6> <Plug>(JavaComplete-Imports-AddMissing)

nmap <F7> <Plug>(JavaComplete-Imports-RemoveUnused)

imap <F7> <Plug>(JavaComplete-Imports-RemoveUnused)


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"default mappings for javacomplete2
nmap <leader>jI <Plug>(JavaComplete-Imports-AddMissing)
nmap <leader>jR <Plug>(JavaComplete-Imports-RemoveUnused)
nmap <leader>ji <Plug>(JavaComplete-Imports-AddSmart)
nmap <leader>jii <Plug>(JavaComplete-Imports-Add)

imap <C-j>I <Plug>(JavaComplete-Imports-AddMissing)
imap <C-j>R <Plug>(JavaComplete-Imports-RemoveUnused)
imap <C-j>i <Plug>(JavaComplete-Imports-AddSmart)
imap <C-j>ii <Plug>(JavaComplete-Imports-Add)

nmap <leader>jM <Plug>(JavaComplete-Generate-AbstractMethods)

imap <C-j>jM <Plug>(JavaComplete-Generate-AbstractMethods)

nmap <leader>jA <Plug>(JavaComplete-Generate-Accessors)
nmap <leader>js <Plug>(JavaComplete-Generate-AccessorSetter)
nmap <leader>jg <Plug>(JavaComplete-Generate-AccessorGetter)
nmap <leader>ja <Plug>(JavaComplete-Generate-AccessorSetterGetter)
nmap <leader>jts <Plug>(JavaComplete-Generate-ToString)
nmap <leader>jeq <Plug>(JavaComplete-Generate-EqualsAndHashCode)
nmap <leader>jc <Plug>(JavaComplete-Generate-Constructor)
nmap <leader>jcc <Plug>(JavaComplete-Generate-DefaultConstructor)

imap <C-j>s <Plug>(JavaComplete-Generate-AccessorSetter)
imap <C-j>g <Plug>(JavaComplete-Generate-AccessorGetter)
imap <C-j>a <Plug>(JavaComplete-Generate-AccessorSetterGetter)

vmap <leader>js <Plug>(JavaComplete-Generate-AccessorSetter)
vmap <leader>jg <Plug>(JavaComplete-Generate-AccessorGetter)
vmap <leader>ja <Plug>(JavaComplete-Generate-AccessorSetterGetter)

nmap <silent> <buffer> <leader>jn <Plug>(JavaComplete-Generate-NewClass)
nmap <silent> <buffer> <leader>jN <Plug>(JavaComplete-Generate-ClassInFile)


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""Plugin key-mappings for snippets

" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"


" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif

let g:tex_conceal = ""
let g:tex_flavor = 'latex'


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vimtex setup

let g:vimtex_view_method = 'zathura'


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ghcup setup

nnoremap <Leader>g :GHCup<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" haskell-vim setup

let g:haskell_classic_highlighting = 1
let g:haskell_indent_if = 3
let g:haskell_indent_case = 2
let g:haskell_indent_let = 4
let g:haskell_indent_where = 6
let g:haskell_indent_before_where = 2
let g:haskell_indent_after_bare_where = 2
let g:haskell_indent_do = 3
let g:haskell_indent_in = 1
let g:haskell_indent_guard = 2
let g:haskell_indent_case_alternative = 1
let g:cabal_indent_section = 2



" to enable highlighting of `forall`
let g:haskell_enable_quantification = 1
" to enable highlighting of `mdo` and `rec`
let g:haskell_enable_recursivedo = 1
" to enable highlighting of `proc`
let g:haskell_enable_arrowsyntax = 1
" to enable highlighting of `pattern`
let g:haskell_enable_pattern_synonyms = 1
" to enable highlighting of type roles
let g:haskell_enable_typeroles = 1
" to enable highlighting of `static`
let g:haskell_enable_static_pointers = 1
" to enable highlighting of backpack keywords
let g:haskell_backpack = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Intero setup

augroup interoMaps
  au!
  " Maps for intero. Restrict to Haskell buffers so the bindings don't collide.

  " Background process and window management
  au FileType haskell nnoremap <silent> <leader>is :InteroStart<CR>
  au FileType haskell nnoremap <silent> <leader>ik :InteroKill<CR>

  " Open intero/GHCi split horizontally
  au FileType haskell nnoremap <silent> <leader>io :InteroOpen<CR>
  " Open intero/GHCi split vertically
  au FileType haskell nnoremap <silent> <leader>iov :InteroOpen<CR><C-W>H
  au FileType haskell nnoremap <silent> <leader>ih :InteroHide<CR>

  " Reloading (pick one)
  " Automatically reload on save
  au BufWritePost *.hs InteroReload
  " Manually save and reload
  au FileType haskell nnoremap <silent> <leader>wr :w \| :InteroReload<CR>

  " Load individual modules
  au FileType haskell nnoremap <silent> <leader>il :InteroLoadCurrentModule<CR>
  au FileType haskell nnoremap <silent> <leader>if :InteroLoadCurrentFile<CR>

  " Type-related information
  " Heads up! These next two differ from the rest.
  au FileType haskell map <silent> <leader>t <Plug>InteroGenericType
  au FileType haskell map <silent> <leader>T <Plug>InteroType
  au FileType haskell nnoremap <silent> <leader>it :InteroTypeInsert<CR>

  " Navigation
  au FileType haskell nnoremap <silent> <leader>jd :InteroGoToDef<CR>

  " Managing targets
  " Prompts you to enter targets (no silent):
  au FileType haskell nnoremap <leader>ist :InteroSetTargets<SPACE>
augroup END

" Intero starts automatically. Set this if you'd like to prevent that.
let g:intero_start_immediately = 0

" Enable type information on hover (when holding cursor at point for ~1 second).
let g:intero_type_on_hover = 1

" Change the intero window size; default is 10.
let g:intero_window_size = 15

" Sets the intero window to split vertically; default is horizontal
let g:intero_vertical_split = 1

" OPTIONAL: Make the update time shorter, so the type info will trigger faster.
set updatetime=1000
