"  ███╗   ██╗██╗   ██╗██╗███╗   ███╗    ██╗███████╗              
"  ████╗  ██║██║   ██║██║████╗ ████║    ██║██╔════╝              
"  ██╔██╗ ██║██║   ██║██║██╔████╔██║    ██║███████╗              
"  ██║╚██╗██║╚██╗ ██╔╝██║██║╚██╔╝██║    ██║╚════██║              
"  ██║ ╚████║ ╚████╔╝ ██║██║ ╚═╝ ██║    ██║███████║              
"  ╚═╝  ╚═══╝  ╚═══╝  ╚═╝╚═╝     ╚═╝    ╚═╝╚══════╝              
"                                                                
"   █████╗ ██╗    ██╗███████╗███████╗ ██████╗ ███╗   ███╗███████╗
"  ██╔══██╗██║    ██║██╔════╝██╔════╝██╔═══██╗████╗ ████║██╔════╝
"  ███████║██║ █╗ ██║█████╗  ███████╗██║   ██║██╔████╔██║█████╗  
"  ██╔══██║██║███╗██║██╔══╝  ╚════██║██║   ██║██║╚██╔╝██║██╔══╝  
"  ██║  ██║╚███╔███╔╝███████╗███████║╚██████╔╝██║ ╚═╝ ██║███████╗
"  ╚═╝  ╚═╝ ╚══╝╚══╝ ╚══════╝╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝



let mapleader = " "

let maplocalleader = " "



" ▄▀▀ ▄▀▄ █▄ ▄█ █▄ ▄█ ▄▀▄ █▄ █ █▀▄ ▄▀▀
" ▀▄▄ ▀▄▀ █ ▀ █ █ ▀ █ █▀█ █ ▀█ █▄▀ ▄██

:command TME TableModeEnable 

:command TMD TableModeDisabled

" Clear the search highlights
" https://stackoverflow.com/questions/657447/vim-clear-last-search-
" highlighting#comment5906101_657484
:command C let @/=""



" ██▄ ▄▀▄ ▄▀▀ █ ▄▀▀   ▄▀▄ █▀▄ ▀█▀ █ ▄▀▄ █▄ █ ▄▀▀
" █▄█ █▀█ ▄██ █ ▀▄▄   ▀▄▀ █▀   █  █ ▀▄▀ █ ▀█ ▄██


set autoindent              " keep indentation of line above

set cc=81                   " 80 column border

set clipboard=unnamedplus   " yanking/deleting to system clipboard

set cursorline              " highlight current line

set expandtab               " convert tabs to white space

set hlsearch                " highlight search results

set ignorecase              " case insensitive matching

set inccommand=nosplit      " Preview commands

set nocompatible            " disable compatibility to old-time vi

set number                  " display line numbers

set shiftwidth=4            " width for auto indents

set showmatch               " show matching brackets

set softtabstop=4           " multiple spaces as tabstops -> correct <BS>

set spell                   " spell checking

set spelllang=de,en,cjk,fr

set tabstop=4               " number of columns occupied by a tab character

set termguicolors

set foldmethod=expr

set foldexpr=VimFolds(v:lnum) " custom folding expression

set foldtext=MyFoldText()

filetype plugin on

filetype plugin indent on   " allows auto-indenting depending on file type

syntax on                   " syntax highlighting



" ▄▀▄ █   ██▀
" █▀█ █▄▄ █▄▄


let g:ale_hover_cursor = 1
let g:ale_set_balloons = 1
let g:airline#extensions#ale#enabled = 1 " pretty ALE status in airline

nmap <silent> <C-k> <Plug>(ale_previous_wrap)   " go to previous error
nmap <silent> <C-j> <Plug>(ale_next_wrap)       " go to next error


" █▀▄ █   █ █ ▄▀  █ █▄ █ ▄▀▀
" █▀  █▄▄ ▀▄█ ▀▄█ █ █ ▀█ ▄██
" handled by vim-plug

call plug#begin('~/.vim/plugged')

" styling nvim
Plug 'reedes/vim-colors-pencil'
Plug 'vim-airline/vim-airline'
Plug 'frazrepo/vim-rainbow'      " rainbow brackets

" git
Plug 'airblade/vim-gitgutter'   " shows git diff in sign column

" navigation
Plug 'preservim/nerdtree'       " file system explorer

" faster writing
Plug 'preservim/nerdcommenter'      " auto commenting via <lead> cc / cu
Plug 'jiangmiao/auto-pairs'         " auto quote and bracket completion
Plug 'tpope/vim-surround'           " change sourroundings cs<old><new> 
Plug 'dhruvasagar/vim-table-mode'   " creating tables with ascii chars 
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
                                    " asynchronous completion framework 
Plug 'sbdchd/neoformat'             " code formatter 

"latex
Plug 'lervag/vimtex'            " compilation, live previews, ...

" linting
Plug 'dense-analysis/ale'       " syntax checking, semantic errors, ...

" haskell
Plug 'ndmitchell/ghcid', { 'rtp': 'plugins/nvim' } " instant error feedback
" Plug 'neovimhaskell/haskell-vim'    " syntax and indentation

call plug#end()


" ▄▀▀ ▄▀▄ █   ▄▀▄ █ █ █▀▄ ▄▀▀
" ▀▄▄ ▀▄▀ █▄▄ ▀▄▀ ▀▄█ █▀▄ ▄██

colorscheme pencil
:set background=dark

let g:pencil_higher_contrast_ui = 1
let g:airline_theme = 'pencil'
let g:airline_powerline_fonts = 1

" enable rainbow brackets
let g:rainbow_active = 1



" ▄▀▀ ▄▀▄ █▄ ▄█ █▄ ▄█ ██▀ █▄ █ ▀█▀ ██▀ █▀▄
" ▀▄▄ ▀▄▀ █ ▀ █ █ ▀ █ █▄▄ █ ▀█  █  █▄▄ █▀▄
" settings for nerdcommenter

let g:NERDSpaceDelims = 1  " always add spaces after comment delimiters
let g:NERDTrimTrailingWhitespace = 1 " when uncommenting



" █▀▄ ██▀ ▄▀▄ █▀▄ █   ██▀ ▀█▀ ██▀
" █▄▀ █▄▄ ▀▄▀ █▀  █▄▄ █▄▄  █  █▄▄

let g:deoplete#enable_at_startup = 1



" █   ▄▀▄ ▀█▀ ██▀ ▀▄▀
" █▄▄ █▀█  █  █▄▄ █ █

let g:vimtex_view_method = 'zathura' " open previews in zahura
let g:tex_flavor = 'latex'



" █▄ █ ██▀ ▄▀▄ █▀ ▄▀▄ █▀▄ █▄ ▄█ ▄▀▄ ▀█▀
" █ ▀█ █▄▄ ▀▄▀ █▀ ▀▄▀ █▀▄ █ ▀ █ █▀█  █ 

let g:neoformat_basic_format_align = 1  " alignment
let g:neoformat_basic_format_retab = 1  " tab to spaces conversion
let g:neoformat_basic_format_trim = 1   " trimmming of trailing whitespace



" █▀ ▄▀▄ █   █▀▄ █ █▄ █ ▄▀ 
" █▀ ▀▄▀ █▄▄ █▄▀ █ █ ▀█ ▀▄█

" https://jdhao.github.io/2019/08/16/nvim_config_folding/
" https://vi.stackexchange.com/questions/3814/is-there-a-best-practice-to-fold-a-vimrc-file

function! VimFolds(lnum)
    " get content of current line and the line below
    let l:cur_line = getline(a:lnum)
    let l:next_line = getline(a:lnum+1)

    if l:cur_line =~# '^"{'
        return '>' . (matchend(l:cur_line, '"{*') - 1)
    else
        if l:cur_line ==# '' && (matchend(l:next_line, '"{*') - 1) == 1
            return 0
        else
            return '='
        endif
    endif
endfunction

function! MyFoldText()
    let line = getline(v:foldstart)
    let folded_line_num = v:foldend - v:foldstart
    let line_text = substitute(line, '^"{\+', '', 'g')
    let nucolwidth = &foldcolumn + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth
    let fillcharcount = windowwidth - len(line_text) - len(folded_line_num) - v:foldlevel - 10
    " return '+'. repeat('-', 4) . line_text . repeat('.', fillcharcount) . ' (' . folded_line_num . ' L)'
    return repeat('.', v:foldlevel) . line_text . '  ' . repeat('.', fillcharcount) . ' (' . folded_line_num . ' L)'
endfunction
