local base_module = {}

function base_module.draw_banner(cr, width, height, hiding_spot_height)
    local x_padding = 32
    local top_padding = 10
    local bottom_padding = 24
    local banner_x_padding = x_padding + 5

    local banner_width = width - 2 * banner_x_padding
    local banner_height= height - top_padding - bottom_padding
    local slit_height = 24
    
    cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND)
    
    -- draw slit
    cairo_set_source_rgba(cr, r, g, b, t_slit)
    cairo_set_line_width(cr, 5)
    cairo_move_to(cr, x_padding, top_padding)
    cairo_rel_line_to(cr, width - 2 * x_padding, 0)
    cairo_stroke(cr)
    
    -- draw banner itself
    cairo_set_operator(cr,CAIRO_OPERATOR_OVER)
    cairo_set_line_width(cr, 2)
    cairo_set_source_rgba(cr, r0, g0, b0, t_banner)
    cairo_move_to(cr, banner_x_padding , top_padding)
    cairo_rel_line_to(cr, 0, banner_height)
    
    local zigzag = -1
    for i=0, banner_width / 4 do
		cairo_rel_line_to(cr, 4, 4 * zigzag)
		zigzag = zigzag * -1
    end

    cairo_rel_line_to(cr, 0, -banner_height + 4)
    cairo_close_path(cr)
    cairo_fill(cr)
    
    --banner border
    cairo_set_line_width(cr, 2)
    cairo_set_source_rgba(cr, r, g, b, t_banner_border)
    cairo_move_to(cr, banner_x_padding, top_padding)
    cairo_rel_line_to(cr, 0, banner_height)
    
    zigzag = -1
    for i=0, banner_width / 4 do
		cairo_rel_line_to(cr, 4, 4 * zigzag)
		zigzag = zigzag * -1
    end

    cairo_rel_line_to(cr, 0, -banner_height + 4)
    cairo_close_path(cr)
    cairo_stroke(cr)
end


function base_module.draw_header(cr, width, height)
    local left_x_offset = 60
    local right_x_offset= width - 130

    local time_y_offset = 76
    local date_y_offset = 102
    local uptime_y_offset = 130

    cairo_select_font_face (cr, "Hasklug", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL)
    cairo_set_source_rgba(cr, r7, g7, b7, t7)
	cairo_set_font_size(cr, 24)
	
    -- time
    ct = cairo_text_extents_t:create()
	cairo_text_extents(cr,conky_parse('${exec date +%H:%M}'),ct)
	cairo_move_to(cr, right_x_offset, time_y_offset)
	cairo_show_text(cr,conky_parse('${exec date +%H:%M}'))
	
	cairo_set_font_size(cr, 16)
    
    -- date
	ct = cairo_text_extents_t:create()
	cairo_text_extents(cr,conky_parse('${exec date +"%A - %d %B"}'),ct)
	cairo_move_to(cr, left_x_offset, date_y_offset)
	cairo_show_text(cr,conky_parse('${exec date +"%A - %d %B"}'))
	
	cairo_set_font_size(cr, 12)
    
    -- uptime
	uptime = conky_parse('${uptime}')
	ct = cairo_text_extents_t:create()
	cairo_text_extents(cr,"Uptime: " .. uptime,ct)
	cairo_move_to(cr, left_x_offset, uptime_y_offset)
	cairo_show_text(cr,"Uptime: " .. uptime)
end

return base_module
