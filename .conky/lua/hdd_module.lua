local hdd_module = {}

function hdd_module.draw_hdd(cr, w, h, circles_per_row, circle_radius, circle_distance, x_offset, circles_x_offset, y_offset)
    local pathname="Root"
    local path="/"

    local fs_used_perc = tonumber(conky_parse("${fs_used_perc " .. path .. "}"))
    local fs_used = conky_parse("${fs_used " .. path .. "}")
    local fs_size = conky_parse("${fs_size " .. path .. "}")

    local hdd_width = 65
    local hdd_height = 75
    local edge_radius = 10

    local holes_y_offset = 15
    local hole_radius = 3

    local tag_width = 35
    local tag_height = 20
    local tag_y_offset = 25

    local num_of_indicators = 4
    local indicator_distance = 2
    local indicator_width = 4
    local indicator_heigth = 7

    local indent_width = num_of_indicators * indicator_width + (num_of_indicators + 3) * indicator_distance
    local indent_height = 9

    cairo_set_source_rgba(cr, r3, g3, b3, t3)
	cairo_set_line_width(cr, 2)
	
    -- hdd outline
    cairo_arc(cr, x_offset + edge_radius, y_offset + edge_radius, edge_radius, math.pi, 3 * math.pi / 2)
	cairo_rel_line_to(cr, hdd_width - 2 * edge_radius, 0)
	cairo_arc(cr, x_offset + hdd_width - edge_radius, y_offset + edge_radius, edge_radius, 3 * math.pi / 2, 2 * math.pi)
	cairo_rel_line_to(cr, 0, hdd_height - 2 * edge_radius)
    cairo_arc(cr, x_offset + hdd_width - edge_radius, y_offset + hdd_height - edge_radius, edge_radius, 0, math.pi / 2)
	
    -- indent
    cairo_rel_line_to(cr, -(hdd_width - indent_width) / 2 + edge_radius, 0)
	cairo_rel_line_to(cr, 0, -indent_height)
	cairo_rel_line_to(cr, -indent_width, 0)
	cairo_rel_line_to(cr, 0, indent_height)
	cairo_rel_line_to(cr, -(hdd_width - indent_width) / 2 + edge_radius, 0)
	
    cairo_arc(cr, x_offset + edge_radius, y_offset + hdd_height - edge_radius, edge_radius, math.pi / 2, math.pi)
	cairo_close_path(cr)
	cairo_stroke(cr)
	
    -- holes
	cairo_arc(cr, x_offset + edge_radius, y_offset + holes_y_offset, hole_radius, 0, 2 * math.pi)
	cairo_arc(cr, x_offset + hdd_width - edge_radius, y_offset + holes_y_offset, hole_radius, 0, 2 * math.pi)
	cairo_fill(cr)
	cairo_arc(cr, x_offset + edge_radius, y_offset + hdd_height - holes_y_offset, hole_radius, 0, 2 * math.pi)
	cairo_arc(cr, x_offset + hdd_width - edge_radius, y_offset + hdd_height - holes_y_offset, hole_radius, 0, 2 * math.pi)
	cairo_fill(cr)
	
    -- name tag frame
    cairo_rectangle (cr, x_offset + (hdd_width - tag_width) / 2, y_offset + tag_y_offset, tag_width, tag_height);
	cairo_stroke(cr)
	
    cairo_set_source_rgba(cr, r7, g7, b7, t7)
    
    -- name tag
	ct = cairo_text_extents_t:create()
	cairo_text_extents(cr, pathname, ct)
    cairo_move_to(cr, x_offset + (hdd_width - tag_width) / 2 + 3, y_offset + tag_y_offset + tag_height - 5)
    cairo_show_text(cr,pathname)

	cairo_set_line_width(cr, 1)
    
    -- pathway indicator
	for i=0, num_of_indicators - 1 do
		cairo_rectangle (cr, x_offset + (hdd_width - indent_width) / 2 + 2 * indicator_distance + i * (indicator_width + indicator_distance), y_offset + hdd_height - indent_height + indicator_distance, indicator_width, indicator_heigth);
		cairo_stroke(cr)
    end

    cairo_set_source_rgba(cr, r7, g7, b7, t7)
    
    for i=0, math.floor(num_of_indicators * fs_used_perc / 100) - 1 do
		cairo_rectangle (cr, x_offset + (hdd_width - indent_width) / 2 + 2 * indicator_distance + i * (indicator_width + indicator_distance), y_offset + hdd_height - indent_height + indicator_distance, indicator_width, indicator_heigth);
		cairo_fill(cr)
    end
	
    -- hdd usage overview
    
	cairo_set_source_rgba(cr, r7, g7, b7, t7_indicator)
    
    for i=0, circles_per_row - 1 do
		cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset + hdd_height / 3, circle_radius, 0, 2 * math.pi)
		cairo_fill(cr)
    end
    
	cairo_set_source_rgba(cr, r3, g3, b3, t3)

    for i=0, math.floor( circles_per_row * fs_used_perc / 100) - 1 do
		cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset + hdd_height / 3, circle_radius, 0, 2 * math.pi)
		cairo_fill(cr)
    end
    
    cairo_set_source_rgba(cr, r7, g7, b7, t7)
    
    -- usage text
	ct = cairo_text_extents_t:create()
	cairo_text_extents(cr, fs_used .. " / " .. fs_size, ct)
    cairo_move_to(cr, circles_x_offset - circle_radius, y_offset + 4 * hdd_height / 5)
    cairo_show_text(cr, fs_used .. " / " .. fs_size)

end

return hdd_module
