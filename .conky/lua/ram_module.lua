local ram_module = {}

function ram_module.draw_ram(cr, w, h, circles_per_row, circle_radius, circle_distance, x_offset, circles_x_offset, y_offset)
    local mem_perc = tonumber(conky_parse("${memperc}"))
    local mem_used = conky_parse("${mem}")
    local mem_max = conky_parse("${memmax}")
    local swap_perc = tonumber(conky_parse("${swapperc}"))
    local swap_used = conky_parse("${swap}")
    local swap_max = conky_parse("${swapmax}")
	
    local ram_width = 72
    local ram_height = 40
    local half_circle_radius = 5

    local indent_x = 42
    local indent_width = 2 * half_circle_radius
    local indent_height = 10

    local pin_heigth = 8
    local pin_distance = 5
    local num_left_pins = 6
    local num_right_pins = 2

    local rectangle_width = 8
    local rectangle_height = 11
    local rectangle_y_offset = 7
    local rectangle_left_offset = 10
    local rectangle_right_offset = 40
    local rectangle_distance = 3
    
    local text_y_offset = y_offset + ram_height + 10
    
    cairo_set_source_rgba(cr, r2, g2, b2, t2)
	cairo_set_line_width(cr, 2)
	
    -- RAM upper outline
	cairo_move_to(cr, x_offset, y_offset + 10)
	cairo_rel_line_to(cr, 0, -10)
	cairo_rel_line_to(cr, ram_width, 0)
	cairo_rel_line_to(cr, 0, 10)
    cairo_stroke(cr)
    
    -- left half-circle
    cairo_arc(cr, x_offset, y_offset + 15, half_circle_radius, 3 * math.pi / 2, math.pi / 2)
    cairo_stroke(cr)
    
    -- right half-circle
    cairo_arc(cr, x_offset + ram_width, y_offset + 15, half_circle_radius, math.pi / 2, 3 * math.pi / 2)
    cairo_stroke(cr)
    
    -- RAM lower outline 
    cairo_move_to(cr, x_offset , y_offset + 20)
    cairo_rel_line_to(cr,0, ram_height - 20)
    cairo_rel_line_to(cr, indent_x, 0)
    cairo_rel_line_to(cr, 0, -indent_height)
    cairo_arc(cr, x_offset + indent_x + half_circle_radius, y_offset + ram_height - indent_height, half_circle_radius, math.pi, 2 * math.pi)
    cairo_rel_line_to(cr, 0, indent_height)
    cairo_rel_line_to(cr, ram_width - indent_x - 2 * half_circle_radius, 0)
    cairo_rel_line_to(cr,0, -ram_height + 20)
    cairo_stroke(cr)    
    
    -- left pins
    for i = 1, num_left_pins do
		cairo_move_to(cr, x_offset + indent_x - i * pin_distance, y_offset + ram_height)
		cairo_rel_line_to(cr, 0, -pin_heigth)
		cairo_stroke(cr)
    end 
    
    -- right pins
    for i=1, num_right_pins do
		cairo_move_to(cr, x_offset + indent_x + 2 * half_circle_radius + i * pin_distance, y_offset + ram_height)
		cairo_rel_line_to(cr, 0, -pin_heigth)
		cairo_stroke(cr)
    end 
    
    -- left rectangles
    cairo_rectangle (cr, x_offset + rectangle_left_offset, y_offset + rectangle_y_offset, rectangle_width, rectangle_height);
    cairo_fill(cr)
    cairo_rectangle (cr, x_offset + rectangle_left_offset + rectangle_width + rectangle_distance, y_offset + rectangle_y_offset, rectangle_width, rectangle_height);
    cairo_fill(cr)  
    
    -- right rectangles
    cairo_rectangle (cr, x_offset + rectangle_right_offset, y_offset + rectangle_y_offset, rectangle_width, rectangle_height);
    cairo_fill(cr)
    cairo_rectangle (cr, x_offset + rectangle_right_offset + rectangle_width + rectangle_distance, y_offset + rectangle_y_offset, rectangle_width, rectangle_height);
    cairo_fill(cr)
    
    cairo_set_source_rgba(cr, r7, g7, b7, t7_indicator)
    
    -- Swap usage overview
    for i = 0, circles_per_row - 1 do
    cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset + ram_height / 5, circle_radius, 0, 2 * math.pi)
    cairo_fill(cr)
    end
    
    cairo_set_source_rgba(cr, r2, g2, b2, t2)
   
    for i=0, math.floor(circles_per_row * swap_perc / 100) do
    cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset + ram_height / 5, circle_radius, 0, 2 * math.pi)
    cairo_fill(cr)
    end
    
    cairo_set_source_rgba(cr, r7, g7, b7, t7_indicator)
    
    -- RAM usage overview
    for i = 0, circles_per_row - 1 do
    cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset + 4 * ram_height / 5, circle_radius, 0, 2 * math.pi)
    cairo_fill(cr)
    end
    
    cairo_set_source_rgba(cr, r2, g2, b2, t2)
    
    for i=0, math.floor(circles_per_row * mem_perc / 100) do
    cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset + 4 * ram_height / 5, circle_radius, 0, 2 * math.pi)
    cairo_fill(cr)
    end
    
    cairo_set_source_rgba(cr, r7, g7, b7, t7)
	ct = cairo_text_extents_t:create()

    -- Swap usage text
	cairo_text_extents(cr, swap_used .. " / " .. swap_max ,ct)
    cairo_move_to(cr, x_offset, text_y_offset + ct.height)
    cairo_show_text(cr, swap_used .. " / " .. swap_max)
    
    -- RAM usage text
	cairo_text_extents(cr, mem_used .. " / " .. mem_max ,ct)
    cairo_move_to(cr, circles_x_offset - circle_radius, text_y_offset + ct.height)
    cairo_show_text(cr, mem_used .. " / " .. mem_max)
    
    cairo_stroke(cr)
end

return ram_module
