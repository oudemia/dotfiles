local gpu_module = {}

function gpu_module.draw_gpu(cr, w, h, x_offset, circles_x_offset, y_offset)
	freq=conky_parse("${exec xrandr | grep '*' | tr ' ' '\n' | grep '*' | tr -d '*' | awk '{printf(\"%d\\\n\",$1 + 0.5)}'}")

    local gpu_width = 75
    local gpu_height = 50
    local gpu_corner = 7

    local module_width = 7
    local small_module_height = 10
    local large_module_height = 20
    local bottom_module_height = 40

    local num_of_lines = 4
    local line_y_offset = y_offset + gpu_height / 2 - num_of_lines * gpu_corner / 2
    local line_width = 15

    local hole_radius = 1

    local fan_outer_radius = 15
    local fan_inner_radius = 7

	local c1=66
	local c2_x= 69 -- (w-c1)/2
	local c2_y= 21 -- (h-c1)/2
	local c3_x= 69 + c1/2 -- w/2
	local c3_y= 600 + c1/2 -- h/2
	
    cairo_set_source_rgba(cr, r6, g6, b6, t6)
	cairo_set_line_width(cr, 2)
	
    -- outline
	cairo_move_to(cr, x_offset, y_offset)
	cairo_rel_line_to(cr, gpu_width, 0)
	cairo_rel_line_to(cr, 0, gpu_height - gpu_corner)
	cairo_rel_line_to(cr, -gpu_corner, gpu_corner)
	cairo_rel_line_to(cr, - gpu_width + gpu_corner, 0)
    cairo_stroke(cr)
	
	cairo_move_to(cr, x_offset - gpu_corner, y_offset - gpu_corner)
	cairo_rel_line_to(cr, gpu_corner, 0)
	cairo_rel_line_to(cr, 0, gpu_height + 2 * gpu_corner)
    cairo_stroke(cr)

    -- side modules
    
	cairo_move_to(cr, x_offset, y_offset + gpu_corner)
	cairo_rel_line_to(cr, -module_width, 0)
	cairo_rel_line_to(cr, 0, small_module_height)
	cairo_rel_line_to(cr, module_width, 0)
    cairo_stroke(cr)
    
	cairo_move_to(cr, x_offset, y_offset + 2 * gpu_corner + small_module_height)
	cairo_rel_line_to(cr, -module_width, 0)
	cairo_rel_line_to(cr, 0, large_module_height)
	cairo_rel_line_to(cr, module_width, 0)
    cairo_stroke(cr)
    
	cairo_move_to(cr, x_offset + gpu_width - 2 * gpu_corner, y_offset + gpu_height)
	cairo_rel_line_to(cr, 0, module_width)
	cairo_rel_line_to(cr, -bottom_module_height, 0)
	cairo_rel_line_to(cr, 0, -module_width)
    cairo_stroke(cr)


    -- lines
    
    cairo_set_line_cap  (cr, CAIRO_LINE_CAP_ROUND);
    for i=0, num_of_lines - 1 do
        cairo_move_to(cr, x_offset + gpu_width - gpu_corner, line_y_offset + i * gpu_corner)
        cairo_rel_line_to(cr, -line_width, 0)
        cairo_stroke(cr)
    end

    -- holes
    cairo_arc(cr, x_offset + gpu_corner + hole_radius, y_offset + gpu_corner + hole_radius, hole_radius, 0, 2 * math.pi)
    cairo_stroke(cr)
    cairo_arc(cr, x_offset + gpu_corner + hole_radius, y_offset + gpu_height - gpu_corner - hole_radius, hole_radius, 0, 2 * math.pi)
    cairo_stroke(cr)
    cairo_arc(cr, x_offset + gpu_width - gpu_corner - hole_radius, y_offset + gpu_height - gpu_corner - hole_radius, hole_radius, 0, 2 * math.pi)
    cairo_stroke(cr)

    cairo_set_source_rgba(cr, r7, g7, b7, t7)
    
    -- fan
    cairo_arc(cr, x_offset + fan_outer_radius + 2 * gpu_corner, y_offset + gpu_height / 2, fan_outer_radius, 0, 2 * math.pi)
    cairo_arc(cr, x_offset + fan_outer_radius + 2 * gpu_corner, y_offset + gpu_height / 2, fan_inner_radius, 0, 2 * math.pi)
    
    -- frequency  text
    cairo_move_to(cr, circles_x_offset, y_offset + gpu_height / 2)
    cairo_show_text(cr,freq .. "Hz")
    cairo_stroke(cr)
end

return gpu_module
