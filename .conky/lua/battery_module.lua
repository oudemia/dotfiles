local battery_module = {}

function battery_module.draw_battery(cr, w, h, circles_per_row, circle_radius, circle_distance, x_offset, circles_x_offset, y_offset)
    local battery_perc_text = conky_parse("${battery_percent}")
    local battery_perc = tonumber(battery_perc_text)
    local battery_time = conky_parse("${battery_time}")
	
    local battery_width = 72
    local battery_height = 30

    local bolt_width = battery_width / 3
    local bolt_height = battery_height + 20
    local bolt_x_offset = (battery_width + bolt_width) / 2
    local bolt_y_offset = y_offset - (bolt_height - battery_height) / 2
    
    local dock_width = 5
    local dock_height = 2 * battery_height / 5
    local dock_corner_radius = 4
    
 	cairo_set_source_rgba(cr, r4, g4, b4, t4)
	cairo_set_line_width(cr, 2)
	
    -- bolt
	cairo_move_to(cr, x_offset + bolt_x_offset, bolt_y_offset)
	cairo_rel_line_to(cr, -bolt_width, 0)
	cairo_rel_line_to(cr, -bolt_width / 5, 3 * bolt_height / 5)
	cairo_rel_line_to(cr, 2 * bolt_width / 3, 0)
	cairo_rel_line_to(cr, -bolt_width / 5, 2 * bolt_height / 5)
	cairo_rel_line_to(cr, bolt_width, -3 * bolt_height / 5)
	cairo_rel_line_to(cr, -2 * bolt_width / 3, 0)
	cairo_close_path(cr)
    if (battery_time == "") then
 	    cairo_set_source_rgba(cr, r7, g7, b7, t7)
        cairo_fill(cr)
 	    cairo_set_source_rgba(cr, r4, g4, b4, t4)
    end
	cairo_stroke(cr)

    -- outline left to bolt
	cairo_move_to(cr, x_offset + battery_width / 2 - bolt_width / 2 - 10, y_offset)
	cairo_rel_line_to(cr, - battery_width / 2 + bolt_width / 2 + 10, 0)
	cairo_rel_line_to(cr, 0, battery_height)
	cairo_rel_line_to(cr, battery_width / 2 - 10, 0)
	cairo_stroke(cr)

    -- outline right to bolt
	cairo_move_to(cr, x_offset + battery_width / 2 + 10, y_offset + battery_height)
	cairo_rel_line_to(cr, battery_width / 2 - 10, 0)
	cairo_rel_line_to(cr, 0, -battery_height)
	cairo_rel_line_to(cr, -battery_width / 2 + bolt_width / 2 + 5, 0)
	cairo_stroke(cr)

	
    -- dock
	cairo_move_to(cr, x_offset + battery_width, y_offset + (battery_height - dock_height) / 2)
	cairo_rel_line_to(cr, dock_width, 0)
	cairo_arc(cr, x_offset + battery_width + dock_width, y_offset + (battery_height - dock_height) / 2 + dock_corner_radius, dock_corner_radius, 3 * math.pi / 2, 0)
	cairo_rel_line_to(cr, 0, dock_height - 2 * dock_corner_radius)
	cairo_arc(cr, x_offset + battery_width + dock_width, y_offset + (battery_height + dock_height) / 2 - dock_corner_radius, dock_corner_radius, 0, math.pi / 2)
	cairo_rel_line_to(cr, -dock_width, 0)
	cairo_stroke(cr)
	
	-- battery overview
    
    cairo_set_source_rgba(cr, r7, g7, b7, t7_indicator)
    
    for i=0, circles_per_row - 1 do
        cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset + circle_radius, circle_radius, 0, 2 * math.pi)
        cairo_fill(cr)
    end
    
    cairo_set_source_rgba(cr, r4, g4, b4, t4)
    
    for i=0, math.floor(circles_per_row * battery_perc / 100) - 1 do
        cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset + circle_radius, circle_radius, 0, 2 * math.pi)
        cairo_fill(cr)
    end

	cairo_set_source_rgba(cr, r7, g7, b7, t7)
    
    --Battery percentage
	cairo_move_to(cr, circles_x_offset - circle_radius, y_offset + battery_height)
	cairo_show_text(cr, battery_perc_text .. "%")
	
	cairo_move_to(cr, circles_x_offset + circles_per_row * circle_distance / 2, y_offset + battery_height)
	cairo_show_text(cr, battery_time)
    
    cairo_stroke(cr)
end

return battery_module
