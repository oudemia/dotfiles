local netspeed_module = {}

function netspeed_module.draw_netspeed(cr, width, height, circles_per_row, circle_radius, circle_distance, x_offset, circles_x_offset, y_offset)
    local max_speed = 2000 -- in KiB with one decimal 
    local upspeed = tonumber(conky_parse("${upspeedf wlp5s0}")) -- in KiB with one decimal 
    local downspeed = tonumber(conky_parse("${downspeedf wlp5s0}")) -- in KiB with one decimal 
    local sig_qual_perc = tonumber(conky_parse("${wireless_link_qual_perc wlp5s0}"))

	local c1=52
	local c2_x= 70 --(w-c1)/2
	local c2_y= 700 --(h-c1)/2
	local c3_x= 70 + c1/2 -- w/2
	local c3_y= 700 + c1/2 -- h/2
	
    local inner_radius = 30
    local outer_radius = 40
    
    local tacho_x_center = x_offset + outer_radius
    local tacho_y_center = y_offset + outer_radius/2

    local outer_indicator_radius = 20
    local middle_indicator_radius = 11
    local inner_indicator_radius = 2
    local indicator_y_center = tacho_y_center + 5

    local good_sig_qual_perc = 80
    local meh_sig_qual_perc = 50
    local bad_sig_qual_perc = 20 

    cairo_set_source_rgba(cr, r5, g5, b5, t5)
    cairo_set_line_width(cr, 2)
    
    -- unused tacho speed rings
	cairo_move_to(cr, tacho_x_center - inner_radius, tacho_y_center)
	cairo_arc(cr,tacho_x_center,tacho_y_center,inner_radius, math.pi , 0)
	cairo_stroke(cr)
	cairo_arc(cr,tacho_x_center,tacho_y_center,outer_radius, math.pi, 0)
	cairo_stroke(cr)
    
    cairo_set_source_rgba(cr, r7, g7, b7, t7)
    cairo_set_line_width(cr, 4)
    
    -- used tacho upspeed ring
    if (upspeed > 0.5) then
	    cairo_arc(cr,tacho_x_center, tacho_y_center, inner_radius, math.pi, -(upspeed / max_speed) * math.pi)
	    cairo_stroke(cr)
    end
    
    -- used tacho downspeed ring
    if (downspeed > 0.5) then
	    cairo_arc(cr,tacho_x_center, tacho_y_center, outer_radius, math.pi, -(downspeed / max_speed) * math.pi)
	    cairo_stroke(cr)
    end
	
    -- signal quality indicator

    cairo_set_source_rgba(cr, r7, g7, b7, t7_indicator)
	cairo_set_line_width(cr, 3)

	cairo_arc(cr, tacho_x_center, indicator_y_center, outer_indicator_radius, 5 * math.pi / 4, 7 * math.pi / 4)
	cairo_stroke(cr)
	cairo_arc(cr, tacho_x_center, indicator_y_center, outer_indicator_radius, 5 * math.pi / 4, 7 * math.pi / 4)
	cairo_stroke(cr)
	cairo_arc(cr, tacho_x_center, indicator_y_center, outer_indicator_radius, 5 * math.pi / 4, 7 * math.pi / 4)
	cairo_stroke(cr)
	
    cairo_set_source_rgba(cr, r7, g7, b7, t7)
	
    if (sig_qual_perc >= good_sig_qual_perc) then
	    cairo_arc(cr, tacho_x_center, indicator_y_center, outer_indicator_radius, 5 * math.pi / 4, 7 * math.pi / 4)
	    cairo_stroke(cr)
    end
    
    if (sig_qual_perc >= meh_sig_qual_perc) then
	    cairo_arc(cr, tacho_x_center, indicator_y_center, middle_indicator_radius, 5 * math.pi / 4, 7 * math.pi / 4)
	    cairo_stroke(cr)
    end
    
    if (sig_qual_perc >= bad_sig_qual_perc) then
	    cairo_arc(cr, tacho_x_center, indicator_y_center, inner_indicator_radius, 5 * math.pi / 4, 7 * math.pi / 4)
	    cairo_stroke(cr)
    end
	
    cairo_set_source_rgba(cr, r7, g7, b7, t7_indicator)
    
    -- speed overview
    
    for i = 0, circles_per_row - 1 do
        cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset - outer_radius / 3, circle_radius, 0, 2 * math.pi)
        cairo_fill(cr)
    end
    
    cairo_set_source_rgba(cr, r5, g5, b5, t5)
    
    for i = 0, math.floor(circles_per_row * sig_qual_perc / 100) - 1 do
        cairo_arc(cr, circles_x_offset + i * circle_distance, y_offset - outer_radius / 3 , circle_radius, 0, 2 * math.pi)
        cairo_fill(cr)
    end

    -- speed info text 

    cairo_set_source_rgba(cr, r7, g7, b7, t7)
	cairo_set_font_size(cr, 12)
    
	ct = cairo_text_extents_t:create()
	cairo_text_extents(cr, "up: " .. upspeed .. " KiB",ct)
	cairo_move_to(cr,circles_x_offset - circle_radius, y_offset + outer_radius / 3)
	cairo_show_text(cr, "up: " .. upspeed .. " KiB")
    
	ct = cairo_text_extents_t:create()
	cairo_text_extents(cr, "down: " .. downspeed .. "KiB",ct)
	cairo_move_to(cr,circles_x_offset + circles_per_row * circle_distance / 2, y_offset + outer_radius / 3)
	cairo_show_text(cr, "down: " .. downspeed .. " KiB")
    
	cairo_stroke(cr)
end

return netspeed_module
