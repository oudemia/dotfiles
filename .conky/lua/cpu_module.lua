local cpu_module = {}

function cpu_module.draw_cpu(cr, w, h, circles_per_row, circle_radius, circle_distance, x_offset, circles_x_offset, y_offset)
    local num_of_cpus = 7
    local cpu_used = {}
    cpu_used[0] = tonumber(conky_parse("${cpu cpu0}"))
    cpu_used[1] = tonumber(conky_parse("${cpu cpu1}"))
    cpu_used[2] = tonumber(conky_parse("${cpu cpu2}"))
    cpu_used[3] = tonumber(conky_parse("${cpu cpu3}"))
    cpu_used[4] = tonumber(conky_parse("${cpu cpu4}"))
    cpu_used[5] = tonumber(conky_parse("${cpu cpu5}"))
    cpu_used[6] = tonumber(conky_parse("${cpu cpu6}"))
    cpu_used[7] = tonumber(conky_parse("${cpu cpu7}"))
	
    local temp = conky_parse('${acpitemp}')
    local freq = conky_parse('${freq_g cpu0}')
    local critical_temp = 80
    
    local border_length = 52
    local pins_per_side = 7
    local pin_lenght = 5
    local pin_distance = 7
    local ring_radius = 20
    local termometer_height = 10
    local info_y_offset = 260

    cairo_set_line_width(cr, 2)
	cairo_set_source_rgba(cr, r1, g1, b1, t1)
	cairo_set_line_join (cr, CAIRO_LINE_JOIN_ROUND);
	
    -- CPU border
	cairo_move_to(cr, x_offset, y_offset)
	cairo_rel_line_to(cr, border_length, 0)
	cairo_rel_line_to(cr, 0, border_length)
	cairo_rel_line_to(cr, -border_length, 0)
    cairo_close_path(cr)
    cairo_stroke(cr)
	
    -- CPU hole
	cairo_arc(cr, x_offset + 6, y_offset + 6, 2, 0, 2 * math.pi)
	cairo_close_path(cr)
    cairo_fill(cr)
	
	cairo_set_line_join (cr, CAIRO_LINE_JOIN_MITER);
    
    -- top pins
    for i = 0, pins_per_side - 1 do
		cairo_move_to(cr, x_offset + pin_lenght + i * pin_distance, y_offset - pin_lenght)
		cairo_rel_line_to(cr, 0, -pin_lenght)
		cairo_close_path(cr)
		cairo_stroke(cr)
    end
    
    -- left pins
    for i = 0, pins_per_side - 1 do
		cairo_move_to(cr, x_offset - pin_lenght, y_offset + pin_lenght + i * pin_distance)
		cairo_rel_line_to(cr, -pin_lenght, 0)
		cairo_close_path(cr)
		cairo_stroke(cr)
    end
    
    -- right pins
    for i = 0, pins_per_side - 1 do
		cairo_move_to(cr, x_offset + border_length + 2 * pin_lenght, y_offset + pin_lenght + i * pin_distance)
		cairo_rel_line_to(cr, -pin_lenght, 0)
		cairo_close_path(cr)
		cairo_stroke(cr)
    end
    
    -- bottom pins
    for i = 0, pins_per_side - 1 do
		cairo_move_to(cr, x_offset + pin_lenght + i * pin_distance, y_offset + border_length + 2 * pin_lenght)
		cairo_rel_line_to(cr, 0, -pin_lenght)
		cairo_close_path(cr)
		cairo_stroke(cr)
    end
    
    -- unused CPU ring
    cairo_set_line_width(cr, 1)
	cairo_arc(cr, x_offset + border_length / 2, y_offset + border_length / 2, ring_radius, 0, 2 * math.pi)
	cairo_stroke(cr)
    
    cairo_set_source_rgba(cr, r7, g7, b7, t7)
    cairo_set_line_width(cr, 4)
    
    -- used CPU ring
	cairo_arc(cr, x_offset + border_length / 2, y_offset + border_length / 2, ring_radius , 3 * math.pi / 2, (3/2 + 2*cpu_used[0]/100) * math.pi)
	cairo_stroke(cr)
	
	cairo_set_line_width(cr, 1)
	cairo_set_source_rgba(cr, r1, g1, b1, t1)
	
    -- termometer outline
	cairo_arc(cr, x_offset + border_length / 2, y_offset + border_length / 2 + termometer_height, 5, 1.7 * math.pi, 1.3*math.pi)
	cairo_rel_line_to(cr, 0, -termometer_height - 2)
	cairo_arc(cr, x_offset + border_length / 2, y_offset + border_length / 2 - termometer_height, 3, math.pi, 2 * math.pi)
	cairo_close_path(cr)
	cairo_stroke(cr)
	
    cairo_set_source_rgba(cr, r7, g7, b7, t7)
	
    -- termometer level
	cairo_arc(cr, x_offset + border_length / 2, y_offset + border_length / 2 + termometer_height, 2, 0, 2 * math.pi)
	cairo_fill(cr)
	cairo_move_to(cr, x_offset + border_length / 2, y_offset + border_length / 2 + termometer_height)
	cairo_rel_line_to(cr, 0, -termometer_height * temp / critical_temp)
	cairo_stroke(cr)
    
    -- CPU sage overview
    cairo_set_source_rgba(cr, r7, g7, b7, t7_indicator)
    
    for i = 0, num_of_cpus - 1 do
        for j=0, circles_per_row - 1 do
            cairo_arc(cr, circles_x_offset + circle_distance * j, y_offset - 20 + i * circle_distance, circle_radius, 0, 2 * math.pi)
            cairo_fill(cr)
        end
    end
   
    cairo_set_source_rgba(cr, r1, g1, b1, t1)
    
    for i = 1, 7 do
        for j=0, math.floor(cpu_used[i] / circles_per_row)  - 1 do
            cairo_arc(cr, circles_x_offset + circle_distance * j, y_offset - 20 + (i-1) * circle_distance, circle_radius, 0, 2 * math.pi)
            cairo_fill(cr)
        end
    end

    -- info text
    cairo_set_source_rgba(cr, r7, g7, b7, t7)
	cairo_set_font_size(cr, 12) 
	ct = cairo_text_extents_t:create()

	cairo_text_extents(cr,temp .. "°C",ct)
	cairo_move_to(cr, x_offset, info_y_offset)
	cairo_show_text(cr,temp .. "°C")
    
	cairo_text_extents(cr,freq .. "Ghz",ct)
	cairo_move_to(cr, x_offset, info_y_offset + 17)
	cairo_show_text(cr,freq .. "Ghz")
	
    cairo_stroke(cr)
end

return cpu_module
