import argparse
from pathlib import Path, PurePath

from PFERD import Pferd
from PFERD.ilias import IliasElementType
from PFERD.transform import (attempt, do, glob, keep, move, move_dir,
                             optionally, re_move, re_rename)

tf_ws_2021_bs = attempt (
    move_dir("OS-Lecture-Videos/", "Vorlesung/Videos/"),
    re_move(r"(\d+)(.*).pdf", "Vorlesung/{}"),
    re_move(r"Übungsblatt (.*) (.*)/(.*)", "Übungsblätter/ÜB_{1}/{3}"),
    re_move(r"Tutoriumsblatt (.*) (.*)/(.*)", "Tutoriumsblätter/TB_{1}/{3}"),
    move_dir("Tutorium P6/", "Tutorium/"),
    move_dir("Related Work/", "Related_Work/"),
    keep,
)


tf_ws_2021_cg = attempt (
    move_dir("Vorlesungsmaterial/", "Vorlesung/"),
    move_dir("Übungen/", "Übung/"),
    keep,
)


def df_ws_2021_or2(path: PurePath, _type: IliasElementType) -> bool:
    if glob("Infos von/*")(path):
        return False
    if glob("Tutorien/")(path):
        return True
    if glob("Tutorien/Tutorium 27, freitags 10 Uhr/")(path):
        return True
    if glob("Tutorien/*")(path):
        return False
    return True

tf_ws_2021_or2 = attempt (
    move_dir("Vorlesung/Unbeschriebene Folien/", "Vorlesung/Folien/"),
    move_dir("Vorlesung/Annotierte Folien/", "Vorlesung/Annotierte_Folien/"),
    move_dir("Vorlesungsaufzeichnungen/", "Vorlesung/Videos/"),
    move_dir("Übungen/", "Übungsblätter/"),
    move_dir("Tutorien/Tutorium 27, freitags 10 Uhr/", "Tutorium/"),
    keep,
)


tf_ws_2021_propa = attempt (
    move_dir("Vorlesungsaufzeichnungen/", "Vorlesung/"),
    move_dir("Tutoriums-Materialien/", "Übung/"),
    keep,
)


def df_ws_2021_scm(path: PurePath, _type: IliasElementType) -> bool:
    if glob("EN/")(path):
        return False
    if glob("VWI-ESTIEM Karlsruhe/")(path):
        return False
    if glob("ROCK YOUR LIFE! Karlsruhe/")(path):
        return False
    return True

tf_ws_2021_scm = attempt (
    move_dir("Vorlesung/", "Vorlesung/Videos/"),
    attempt(
        re_move(r"DE/Blatt (\d+).pdf", "Übung/Blatt_{1:0>2}.pdf"),
        re_move(r"DE/Blatt (\d+)-Loesung.pdf", "Übung/Blatt_{1:0>2}-ML.pdf"),
        move_dir("DE/", "Vorlesung/"),
    ),
    keep,
)



def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("--test-run", action="store_true")
    parser.add_argument("synchronizers", nargs="*")
    args = parser.parse_args()

    pferd = Pferd(Path(__file__).parent, test_run=args.test_run)
    pferd.enable_logging()

    if not args.synchronizers or "bs" in args.synchronizers:
        pferd.ilias_kit(
            target="Betriebssysteme",
            course_id="1240138",
            transform=tf_ws_2021_bs,
            cookies="ilias_cookies.txt",
        )

    if not args.synchronizers or "cg" in args.synchronizers:
        pferd.ilias_kit(
            target="Computergrafik",
            course_id="1281232",
            transform=tf_ws_2021_cg,
            cookies="ilias_cookies.txt",
        )

    if not args.synchronizers or "or2" in args.synchronizers:
        pferd.ilias_kit(
            target="Einführung_II",
            course_id="1269217",
            transform=tf_ws_2021_or2,
            dir_filter=df_ws_2021_or2,
            cookies="ilias_cookies.txt",
        )

    if not args.synchronizers or "propa" in args.synchronizers:
        pferd.ilias_kit(
            target="Programmierparadigmen",
            course_id="1261491",
            transform=tf_ws_2021_propa,
            cookies="ilias_cookies.txt",
        )

    if not args.synchronizers or "scm" in args.synchronizers:
        pferd.ilias_kit(
            target="Strategisches_SCM",
            course_id="1266221",
            transform=tf_ws_2021_scm,
            dir_filter=df_ws_2021_scm,
            cookies="ilias_cookies.txt",
        )

    # Prints a summary listing all new, modified or deleted files
    pferd.print_summary()

if __name__ == "__main__":
    main()
