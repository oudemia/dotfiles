#!/bin/bash

ACTIVE_ICON=" "
INACTIVE_ICON=" "

status=`xset -q | grep 'DPMS is' | awk '{ print $3 }'`

getIcon() {
    if [ $status == 'Enabled' ]; then
	    icon=$INACTIVE_ICON
    else
	    icon=$ACTIVE_ICON
	fi
	echo "$icon"
}

case $1 in 
  toggle) 
    if [ $status == 'Enabled' ]; then
        xset -dpms && \
        dunstify 'Screen suspend is disabled.'
    else
   	    xset +dpms && \
        dunstify 'Screen suspend is enabled.'
    fi
    ;;
  display)
    printf "$(getIcon)" 
    ;;
esac
