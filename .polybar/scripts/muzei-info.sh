#!/bin/sh

get_text() {
    artist=$(cat /home/oudemia/coding/muzei-desktop/by.txt)
    title=$(cat /home/oudemia/coding/muzei-desktop/title.txt)

    echo "$title -- $artist"
}

case $1 in 
    openlink) 
        link=$(cat /home/oudemia/coding/muzei-desktop/details.txt)
        firefox -new-window "$link"
        ;;
    display)
        printf "$(get_text)" 
        ;;
esac
