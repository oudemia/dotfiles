#!/usr/bin/env bash

DIR="$HOME/.polybar"

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch the bar
# polybar -q main -c "$DIR"/config_hell.ini &

# Launch all bars on all monitors

if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    echo "---" | tee -a /tmp/polybar_c.log /tmp/polybar_l.log /tmp/polybar_m.log /tmp/polybar_s.log
     MONITOR=$m polybar -c "$DIR"/config_hell.ini control 2>&1 | tee -a /tmp/polybar_c.log & disown
     MONITOR=$m polybar -c "$DIR"/config_hell.ini launch 2>&1 | tee -a /tmp/polybar_l.log & disown
     MONITOR=$m polybar -c "$DIR"/config_hell.ini music 2>&1 | tee -a /tmp/polybar_m.log & disown
     MONITOR=$m polybar -c "$DIR"/config_hell.ini system 2>&1 | tee -a /tmp/polybar_s.log & disown
  done
else
    echo "---" | tee -a /tmp/polybar_c.log /tmp/polybar_l.log /tmp/polybar_m.log /tmp/polybar_s.log
     polybar -c "$DIR"/config_hell.ini control 2>&1 | tee -a /tmp/polybar_c.log & disown
     polybar -c "$DIR"/config_hell.ini launch 2>&1 | tee -a /tmp/polybar_l.log & disown
     polybar -c "$DIR"/config_hell.ini music 2>&1 | tee -a /tmp/polybar_m.log & disown
     polybar -c "$DIR"/config_hell.ini system 2>&1 | tee -a /tmp/polybar_s.log & disown
fi
echo "Polybar launched..."
