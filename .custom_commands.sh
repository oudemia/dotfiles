#!/bin/bash

function crun() {
    ADJUSTED_NAME="${1%.c}"
    if [[ $ADJUSTED_NAME == $1 ]]; then
        echo "Executable name matches input file. I refuse to destroy the original file!"
        return 1
    fi
    clang -pedantic -Wall -Wextra -lm "$1" -o "$ADJUSTED_NAME" && "./$ADJUSTED_NAME"
}

function spotyload() {
    /usr/lib/jvm/java-8-openjdk/bin/java -jar \
         ~/coding/SpotifyDownloader/SpotifyDownloaderJVM.jar \
        --client_secret=0f02b7c483c04257984695007a4a8d5c \
        --client_id=4fe3fecfe5334023a1472516cc99d805
}

function countdown(){
   date1=$((`date +%s` + $1)); 
   while [ "$date1" -ge `date +%s` ]; do 
     echo -ne "$(date -u --date @$(($date1 - `date +%s`)) +%H:%M:%S)\r";
     sleep 0.1
   done
}

function stopwatch(){
  date1=`date +%s`; 
   while true; do 
    echo -ne "$(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)\r"; 
    sleep 0.1
   done
}


function colorcodes() {
    for code ({000..255}) print -P -- "$code: %F{$code}I am a beautiful color%f"
}

# https://stackoverflow.com/questions/5132749/diff-an-image-using-imagemagick
function imgcomp() {
    comp_dir="image_compares"

    if [ ! -d "$comp_dir" ]; then
        echo "creating $comp_dir directory..." 
        mkdir "$comp_dir"
    fi

    for file in "$1"/*
    do
        base=$(basename $file)
        if [[ -f "$2/$base" ]]; then
            compare "$1/$base" "$2/$base" -compose src "$comp_dir/$base"
            echo "comparing $file..."
        fi
    done
}

function vidlen() {
    ffmpeg -i $1 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//
}
