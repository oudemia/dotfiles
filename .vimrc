filetype plugin on

set ruler

set number

syntax enable

set cursorline

set autoindent

set laststatus=2
set t_Co=256

set nocompatible              " be iMproved, required

filetype off                  " required

set termguicolors

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'WolfgangMehner/c-support'

Plugin 'vim-airline/vim-airline'

Plugin 'vim-airline/vim-airline-themes'

Plugin 'abnt713/vim-hashpunk'

Plugin 'cseelus/vim-colors-lucid'
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal


"fun with colors

colorscheme lucid

let g:airline_theme='luna'
let g:airline_powerline_fonts = 1

